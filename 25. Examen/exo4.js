'use strict'

let presentation;
let information;
let moncv;
let contact;
//let change;


document.addEventListener('DOMContentLoaded', () => {


    presentation = document.querySelector('[data-id="presentation"]');
    information = document.querySelector('[data-id="information"]');
    moncv = document.querySelector('[data-id="moncv"]');
    contact = document.querySelector('[data-id="contact"]');
    //presentation.addEventListener('click', change);
    /*information = document.querySelector("#information");
    moncv = document.querySelector("#moncv");
    contact = document.querySelector("#contact");
    */
    presentation.addEventListener('click', changePresentation);
    information.addEventListener('click', changeInformation);
    moncv.addEventListener('click', changeMoncv);
    contact.addEventListener('click', changeContact);
    
});


function changePresentation() {

    information.classList.remove('current');
    moncv.classList.remove('current');
    contact.classList.remove('current');

    if(presentation.classList == 'current') {
        presentation.classList.remove('current');
    } else {
        presentation.classList.add('current');       
    }   
}

function changeInformation() {

    presentation.classList.remove('current');
    moncv.classList.remove('current');
    contact.classList.remove('current');

    if(information.classList == 'current') {
        information.classList.remove('current');
    } else {
        information.classList.add('current');
        
    }
    
}

function changeMoncv() {

    information.classList.remove('current');
    presentation.classList.remove('current');
    contact.classList.remove('current');
    if(moncv.classList == 'current') {
        moncv.classList.remove('current');
    } else {
        moncv.classList.add('current');
        
    }
}

function changeContact() {

    information.classList.remove('current');
    moncv.classList.remove('current');
    presentation.classList.remove('current');

    if(contact.classList == 'current') {
        contact.classList.remove('current');
    } else {
        contact.classList.add('current');
        
    }
    
}